package structural.decorator_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public class ConcreteDecorator extends Decorator{
    public ConcreteDecorator(Component component) {
        super(component);
    }
}
