package structural.decorator_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Component component = new ConcreteComponent();
        Decorator decorator = new ConcreteDecorator(component);

        System.out.println("component");
        component.baseOperation();
        System.out.println();
        System.out.println("decorator");
        decorator.baseOperation();

    }
}
