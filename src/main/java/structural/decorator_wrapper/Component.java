package structural.decorator_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public interface Component {
    void baseOperation();
}
