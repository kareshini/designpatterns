package structural.decorator_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public class ConcreteComponent implements Component {
    @Override
    public void baseOperation() {
        System.out.println("base operation form Component");
    }
}
