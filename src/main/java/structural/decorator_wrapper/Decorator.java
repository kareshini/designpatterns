package structural.decorator_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public abstract class Decorator implements Component{
    private Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void baseOperation() {
        component.baseOperation();
        System.out.println("addition operation which is like overlay to former method");
    }
}
