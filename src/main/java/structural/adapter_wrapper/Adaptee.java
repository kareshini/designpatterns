package structural.adapter_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public interface Adaptee {
    int incompatibleOperation();
}
