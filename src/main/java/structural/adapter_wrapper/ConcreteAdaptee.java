package structural.adapter_wrapper;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteAdaptee implements Adaptee {
    @Override
    public int incompatibleOperation() {
        return 1;
    }
}
