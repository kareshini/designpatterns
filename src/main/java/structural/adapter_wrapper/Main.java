package structural.adapter_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Target target = new Adapter(new ConcreteAdaptee());
        System.out.println(target.someOperation());
    }
}
