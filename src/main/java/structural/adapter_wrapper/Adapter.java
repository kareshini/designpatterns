package structural.adapter_wrapper;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Adapter implements Target {
    private Adaptee adaptee ;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    public String someOperation() {
        int tmp = adaptee.incompatibleOperation();
        return String.valueOf(tmp);
    }
}
