package structural.facade;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Facade {
    private Subsystem1 subsystem1;
    private Subsystem2 subsystem2;

    public Facade(Subsystem1 subsystem1, Subsystem2 subsystem2) {
        this.subsystem1 = subsystem1;
        this.subsystem2 = subsystem2;
    }

    public void doSth(){
        System.out.println("Facade method");
        System.out.println();
        subsystem1.operation();
        subsystem2.operation();
    }
}
