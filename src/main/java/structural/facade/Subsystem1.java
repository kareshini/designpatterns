package structural.facade;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Subsystem1 {
    public void operation(){
        System.out.println("subsystem 1");
    }
}
