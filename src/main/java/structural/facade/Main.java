package structural.facade;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Subsystem1 element1 = new Subsystem1();
        Subsystem2 element2 = new Subsystem2();

        Facade facade = new Facade(element1,element2);
        facade.doSth();
    }
}
