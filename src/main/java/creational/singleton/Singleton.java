package creational.singleton;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Singleton {
    private static volatile Singleton instance = new Singleton();

    private Singleton(){

    }
    public static Singleton getInstance(){
        return instance;
    }

    public void doSth(){
        System.out.println("doSth method");
    }
}
