package creational.singleton;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance();
        singleton.doSth();
    }

}
