package creational.factory_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public interface Shape {
    void draw();
}
