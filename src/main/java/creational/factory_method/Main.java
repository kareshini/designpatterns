package creational.factory_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Main {
    public static void main(String[] args) {
        ShapeFactory factory = new ShapeFactory();

        factory.getShape("Rectangle").draw();
        factory.getShape("Circle").draw();
        factory.getShape("Triangle").draw();
    }
}
