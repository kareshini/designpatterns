package creational.factory_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ShapeFactory {
    public Shape getShape(String shapeType){
        switch (shapeType.toLowerCase()){
            case "circle":
                return new Circle();
            case "rectangle":
                return new Rectangle();
            case "triangle":
                return new Triangle();
            default:
                return null;
        }
    }
}
