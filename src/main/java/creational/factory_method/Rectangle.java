package creational.factory_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Draw: Rectangle");
    }
}
