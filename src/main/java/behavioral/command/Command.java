package behavioral.command;

/**
 * Created by Dominik on 2016-12-21.
 */
public interface Command {
    void execute();
}
