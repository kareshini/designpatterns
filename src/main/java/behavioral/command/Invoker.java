package behavioral.command;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Invoker {
    private List<Command> commands = new LinkedList<>();

    public void setNewCommand(Command command) {
        commands.add(command);
    }
    public void executeAllCommands(){
        commands.forEach(e->e.execute());
        commands.clear();
    }
}
