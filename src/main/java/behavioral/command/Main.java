package behavioral.command;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Main {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();
        TargetReceiver target = new TargetReceiver();

        ConcreteCommand command1 = new ConcreteCommand(target);
        ConcreteCommand command2 = new ConcreteCommand(target);

        invoker.setNewCommand(command1);
        invoker.setNewCommand(command2);

        invoker.executeAllCommands();
    }
}
