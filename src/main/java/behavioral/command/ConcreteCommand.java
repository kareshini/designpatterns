package behavioral.command;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteCommand implements Command {
    private TargetReceiver receiver;

    public ConcreteCommand(TargetReceiver receiver) { //nastepnie do command factory wydzielic
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.doSth();
    }
}
