package behavioral.state;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context(new ConcreteStateA());

        context.request();
        context.request();
    }
}
