package behavioral.state;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Context {
    private State state;

    public Context(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        System.out.println("State changed from "  + this.state + " into " + state);
        this.state = state;
    }

    public void request(){
        state.handle(this);
    }
}
