package behavioral.state;

/**
 * Created by Dominik on 2016-12-21.
 */
public interface State {
    void handle(Context context);
}
