package behavioral.state;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteStateA implements State {
    @Override
    public void handle(Context context) {
        context.setState(new ConcreteStateB());
    }
}
