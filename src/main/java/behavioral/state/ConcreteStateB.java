package behavioral.state;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteStateB implements State {
    @Override
    public void handle(Context context) {
        context.setState(new ConcreteStateA());
    }
}
