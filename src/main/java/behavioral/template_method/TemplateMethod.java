package behavioral.template_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public abstract class TemplateMethod {
    public abstract void method1();
    public abstract void method2();

    public void templateMethod(){
        method1();
        method2();
    }
}
