package behavioral.template_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Main {
    public static void main(String[] args) {
        TemplateMethod temp = new ConcreteTemplateMethodA();
        temp.templateMethod();

        temp = new ConcreteTemplateMethodB();
        temp.templateMethod();
    }
}
