package behavioral.template_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteTemplateMethodA extends TemplateMethod {
    @Override
    public void method1() {
        System.out.println("Method 1 : A");
    }

    @Override
    public void method2() {
        System.out.println("Method 2 : A");
    }
}
