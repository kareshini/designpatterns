package behavioral.template_method;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteTemplateMethodB extends TemplateMethod {
    @Override
    public void method1() {
        System.out.println("Method 1 : B");
    }

    @Override
    public void method2() {
        System.out.println("Method 2 : B");
    }
}
