package behavioral.observer_listener.version1_observer_pull;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Subject {
    private List<Observer> observers = new LinkedList<>();
    private int someState = 0 ;

    public void registerObserver(Observer observer){

        observers.add(observer);
    }
    public void unregisterObserver(Observer observer){
        observers.remove(observer);
    }

    private void notifyObservators(){
        observers.stream().forEach( o -> o.update() );
    }

    public int getSomeState() {
        return someState;
    }

    public void setSomeState(int someState) {
        this.someState = someState;
        notifyObservators();
    }

}
