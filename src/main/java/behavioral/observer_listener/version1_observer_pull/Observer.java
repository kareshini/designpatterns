package behavioral.observer_listener.version1_observer_pull;

/**
 * Created by Dominik on 2016-12-20.
 */
public abstract class Observer {
    protected Subject subject;
    public abstract void update();
}
