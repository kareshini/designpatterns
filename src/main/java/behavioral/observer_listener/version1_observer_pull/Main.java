package behavioral.observer_listener.version1_observer_pull;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Subject sub = new Subject();
        Observer obs1 = new ConcreteObserver(sub);
        Observer obs2 = new ConcreteObserver(sub);

        sub.setSomeState(10);
    }
}
