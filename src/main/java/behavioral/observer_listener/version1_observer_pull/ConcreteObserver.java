package behavioral.observer_listener.version1_observer_pull;

/**
 * Created by Dominik on 2016-12-20.
 */
public class ConcreteObserver extends Observer {
    public ConcreteObserver(Subject subject) {
        this.subject = subject;
        subject.registerObserver(this);
    }

    @Override
    public void update() {
        System.out.println("ConcreteObserver: new value of subject = " + subject.getSomeState());
    }
}
