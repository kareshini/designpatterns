package behavioral.observer_listener.version3_listener;

/**
 * Created by Dominik on 2016-12-20.
 */
public class ConcreteListener implements  Listener {
    @Override
    public void notified() {
        System.out.println("Notified changes");
    }
}
