package behavioral.observer_listener.version3_listener;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Subject {
    private List<Listener> listeners = new LinkedList<>();

    public void registerListener(Listener observer){

        listeners.add(observer);
    }
    public void unregisterListener(Listener observer){
        listeners.remove(observer);
    }

    public void notifyListeners(){
        listeners.forEach( e -> e.notified());
    }
}
