package behavioral.observer_listener.version3_listener;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Subject subject = new Subject();
        Listener listener1 = new ConcreteListener();
        Listener listener2 = new ConcreteListener();

        subject.registerListener(listener1);
        subject.registerListener(listener2);

        subject.notifyListeners();
    }
}
