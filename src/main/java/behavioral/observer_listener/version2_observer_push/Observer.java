package behavioral.observer_listener.version2_observer_push;

/**
 * Created by Dominik on 2016-12-20.
 */
public interface Observer {
    void update(Subject subject);
}
