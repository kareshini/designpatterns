package behavioral.observer_listener.version2_observer_push;

/**
 * Created by Dominik on 2016-12-20.
 */
public class Main {
    public static void main(String[] args) {
        Subject sub = new Subject();
        Observer obs1 = new ConcreteObserver();
        Observer obs2 = new ConcreteObserver();

        sub.registerObserver(obs1);
        sub.registerObserver(obs2);

        sub.setSomeState(10);
    }
}
