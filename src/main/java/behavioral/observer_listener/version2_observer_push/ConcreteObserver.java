package behavioral.observer_listener.version2_observer_push;

/**
 * Created by Dominik on 2016-12-20.
 */
public class ConcreteObserver implements Observer {
    @Override
    public void update(Subject subject) {
        System.out.println("ConcreteObserver: new value of subject = " + subject.getSomeState());
    }
}
