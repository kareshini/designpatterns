package behavioral.visitor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ElementsContainer implements Element {
    private List<Element> elements = new LinkedList<>();

    public void addElement(Element element){
        elements.add(element);
    }
    public void removeElement(Element element){
        elements.remove(element);
    }
    @Override
    public void accept(Visitor visitor) {
        elements.forEach( e -> e.accept(visitor));
    }
}
