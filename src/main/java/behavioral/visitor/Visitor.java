package behavioral.visitor;

/**
 * Created by Dominik on 2016-12-21.
 */
public interface Visitor {
    void visitElementA(ConcreteElementA elementA);
    void visitElementB(ConcreteElementB elementB);
}
