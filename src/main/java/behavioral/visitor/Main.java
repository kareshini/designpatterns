package behavioral.visitor;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Main {
    public static void main(String[] args) {
        ElementsContainer container = new ElementsContainer();

        container.addElement(new ConcreteElementA());
        container.addElement(new ConcreteElementA());
        container.addElement(new ConcreteElementB());

        container.accept(new ConcreteVisitorA());
        container.accept(new ConcreteVisitorB());
    }
}
