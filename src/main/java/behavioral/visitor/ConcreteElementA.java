package behavioral.visitor;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteElementA implements Element{
    @Override
    public void accept(Visitor visitor) {
        visitor.visitElementA(this);
    }
}
