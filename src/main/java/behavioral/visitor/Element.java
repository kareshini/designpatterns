package behavioral.visitor;

/**
 * Created by Dominik on 2016-12-21.
 */
public interface Element {
    void accept(Visitor visitor);
}
