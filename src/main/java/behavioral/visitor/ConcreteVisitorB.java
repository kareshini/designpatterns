package behavioral.visitor;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteVisitorB implements Visitor{

    @Override
    public void visitElementA(ConcreteElementA elementA) {
        System.out.println("Visitor B visited Element A");
    }

    @Override
    public void visitElementB(ConcreteElementB elementB) {
        System.out.println("Visitor B visited Element B");
    }
}
