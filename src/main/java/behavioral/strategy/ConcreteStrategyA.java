package behavioral.strategy;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteStrategyA implements Strategy {
    @Override
    public void performAlgorithm() {
        System.out.println("Strategy A");
    }
}
