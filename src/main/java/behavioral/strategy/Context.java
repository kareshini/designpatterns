package behavioral.strategy;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Context {
    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }
    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void performAlgorithm(){
        strategy.performAlgorithm();
    }
}
