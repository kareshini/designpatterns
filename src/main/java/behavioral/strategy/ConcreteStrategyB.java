package behavioral.strategy;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteStrategyB implements Strategy {
    @Override
    public void performAlgorithm() {
        System.out.println("Strategy B");
    }
}
