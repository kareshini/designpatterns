package behavioral.strategy;

/**
 * Created by Dominik on 2016-12-21.
 */
public interface Strategy {
    void performAlgorithm();
}
