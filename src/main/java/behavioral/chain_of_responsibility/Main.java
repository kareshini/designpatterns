package behavioral.chain_of_responsibility;

/**
 * Created by Dominik on 2016-12-21.
 */
public class Main {
    public static void main(String[] args) {
        Handler a = new ConcreteHandlerA();
        Handler b = new ConcreteHandlerB();

        a.setNext(b);
        b.setNext(a);

        for(int i = -10 ; i <= 10 ; i++){
            a.handleRequest(i);
        }
    }
}
