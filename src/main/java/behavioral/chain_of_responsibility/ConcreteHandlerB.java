package behavioral.chain_of_responsibility;

/**
 * Created by Dominik on 2016-12-21.
 */
public class ConcreteHandlerB extends Handler{
    @Override
    public void handleRequest(int n) {
        if(n>=0){
            System.out.println("Handling through B: " + n);
        }else{
            if(next!=null) {
                System.out.println("I cant handle (B) delegate to next");
                next.handleRequest(n);
            }else{
                System.out.println("Unable to handle this request");
            }
        }
    }
}
