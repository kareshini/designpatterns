package behavioral.chain_of_responsibility;

/**
 * Created by Dominik on 2016-12-21.
 */
public abstract class Handler {
    protected Handler next;

    public void setNext(Handler next){
        this.next=next;
    }

    public abstract void handleRequest(int n); //beter is to send object REQUEST

}
